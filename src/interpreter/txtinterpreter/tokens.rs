#[derive(Debug)]
#[derive(Clone)]
#[derive(PartialEq)]
pub enum Token
{
    Add,
    Mul,
    Sub,
    Div,
    Mod,

    OpenPar,
    GroupPar,
    ClosePar,
    
    Lt,
    Le,
    Gt,
    Ge,
    Eq,
    Ne,
    Not,

    Var(String),
    Number(f64),
    String(String),
    True,
    False
}
