use super::tokens::Token;

pub struct Lexer
{
}

impl Lexer
{
    pub fn new() -> Lexer
    {
	return Lexer {
	};
    }

    pub fn scan(&self, text : &str) -> Vec<Token>
    {
	let mut tokens : Vec<Token> = Vec::new();
	let separators = vec! [
	    " ", "$",
	    "+", "-", "*", "/", "%", "(", ")"
	];

	let reserved = vec![
	    "lt", "le", "gt", "ge", "+", "-", "*", "/", "%",
	    "eq", "ne", "(", ")", "not", "true", "false"
	];
	
	let mut strtok : Vec<String> = vec![];
	let mut buf = String::new();
	let mut c_prec : char = ' ';
	let regex_digit = regex::Regex::new(r"[a-zA-Z_0-9.]").unwrap();
	
	for (i, c) in text.chars().enumerate()
	{
	    if regex_digit.is_match(c_prec.to_string().as_str())
	    {
		if !regex_digit.is_match(c.to_string().as_str())
		{
		    let b = buf.clone().replace(" ", "");

		    if !b.is_empty()
		    {
			strtok.push(b);
		    }

		    buf.clear();
		}
	    }
	    
	    if i >= text.len() - 1 || separators.contains(&format!("{}", c).as_str())
	    {				
		buf.push_str(format!("{}", c).as_str());
		
		let b = buf.clone().replace(" ", "");

		if !b.is_empty()
		{
		    strtok.push(b);
		}
		
		buf.clear();
	    }
	    else
	    {
		buf.push_str(format!("{}", c).as_str());
	    }

	    c_prec = c;
	}

	let mut s_prec = String::new();
	let regex_var_name = regex::Regex::new("[_a-zA-Z][_a-zA-Z0-9]*").unwrap();
	let regex_ip_ok = regex::Regex::new(r"^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$").unwrap();
	
	for s in strtok.iter()
	{
	    if s_prec == "$"
	    {
		if regex_var_name.is_match(s)
		{
		    tokens.push(Token::Var(s.clone()));
		}
	    }
	    
	    if s_prec != "$"
	    {
		if regex_var_name.is_match(s) && !reserved.contains(&s.as_str())
		{
		    tokens.push(Token::String(s.to_string()));
		}
	    }
	    
	    if let Ok(n) = s.parse::<f64>()
	    {
		tokens.push(Token::Number(n));
	    }
	    
	    if regex_ip_ok.is_match(s.as_str())
	    {
		tokens.push(Token::String(s.to_string()));
	    }

	    let mut nb_dots : u32 = 0;
	    for c in s.chars() { if c == '.' { nb_dots += 1; } }
	    
	    if nb_dots != 0 && nb_dots != 1 && nb_dots != 3
	    {
		panic!("IP Address bad format : {}", s);
	    }
	    
	    match s.as_str()
	    {
		"lt" => { tokens.push(Token::Lt); },
		"le" => { tokens.push(Token::Le); },
		"gt" => { tokens.push(Token::Gt); },
		"ge" => { tokens.push(Token::Ge); },
		"eq" => { tokens.push(Token::Eq); },
		"ne" => { tokens.push(Token::Ne); },
		"not" => { tokens.push(Token::Not); },
		"+" => { tokens.push(Token::Add); },
		"-" => { tokens.push(Token::Sub); },
		"*" => { tokens.push(Token::Mul); },
		"/" => { tokens.push(Token::Div); },
		"%" => { tokens.push(Token::Mod); },
		"(" => { tokens.push(Token::OpenPar); },
		")" => { tokens.push(Token::ClosePar); },
		"true" => { tokens.push(Token::True); },
		"false" => { tokens.push(Token::False); },
		_ => {}
	    }	    

	    s_prec = s.clone();
	}

	if s_prec == "$"
	{
	    panic!("Missing var name.");
	}

	// Concat consecutive strings
	let mut single_strings : Vec<Token> = Vec::new();
	let mut current_string = String::new();
	
	for tok in tokens
	{
	    match tok
	    {
		Token::String(s) => {
		    if current_string.is_empty()
		    {
			current_string += format!("{}", s).as_str();
		    }
		    else
		    {
			current_string += format!(" {}", s).as_str();
		    }
		},

		_ => {
		    if !current_string.is_empty()
		    {
			single_strings.push(Token::String(current_string));
			current_string = String::new();
		    }
		    
		    single_strings.push(tok);
		}
	    }
	}

	if !current_string.is_empty()
	{
	    single_strings.push(Token::String(current_string));
	}

	return single_strings;
    }
    
}


#[cfg(test)]
mod tests
{
    use super::Token;
    use super::Lexer;
    
    // scan var //
    #[test]
    fn scan_var_one()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("$hello");

	assert_eq!(1, tokens.len());
	
	assert!(match tokens[0].clone() {
	    Token::Var(x) => {
		x == "hello"
	    },

	    _ => { false }
	});
    }
    
    // scan number //
    #[test]
    fn scan_int_one()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("123456");

	assert_eq!(1, tokens.len());
	
	assert!(match tokens[0].clone() {
	    Token::Number(x) => {
		x == 123456.0
	    },

	    _ => { false }
	});
    }

    #[test]
    fn scan_int_par()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("(123456)");

	assert_eq!(3, tokens.len());
	
	assert!(match tokens[1].clone() {
	    Token::Number(x) => {
		x == 123456.0
	    },

	    _ => { false }
	});

	assert!(match tokens[0].clone() {
	    Token::OpenPar => {
		true
	    },

	    _ => { false }
	});

	assert!(match tokens[2].clone() {
	    Token::ClosePar => {
		true
	    },

	    _ => { false }
	});

    }

    #[test]
    fn scan_float_one()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("3.142");

	assert_eq!(1, tokens.len());
	
	assert!(match tokens[0].clone() {
	    Token::Number(x) => {
		x == 3.142
	    },

	    _ => { false }
	});
    }
    
    // scan arith //
    #[test]
    fn scan_arith_usub()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("-4");

	assert_eq!(2, tokens.len());
	assert_eq!(Token::Sub, tokens[0]);
	assert_eq!(Token::Number(4.0), tokens[1]);
    }
    
    #[test]
    fn scan_arith_add()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("1 + 2 + 3");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(1.0), tokens[0]);
	assert_eq!(Token::Add, tokens[1]);
	assert_eq!(Token::Number(2.0), tokens[2]);
	assert_eq!(Token::Add, tokens[3]);
	assert_eq!(Token::Number(3.0), tokens[4]);
    }

    #[test]
    fn scan_arith_sub()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("5 - 3 - 1");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(5.0), tokens[0]);
	assert_eq!(Token::Sub, tokens[1]);
	assert_eq!(Token::Number(3.0), tokens[2]);
	assert_eq!(Token::Sub, tokens[3]);
	assert_eq!(Token::Number(1.0), tokens[4]);
    }
    
    #[test]
    fn scan_arith_mul()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("2 * 4 * 6");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(2.0), tokens[0]);
	assert_eq!(Token::Mul, tokens[1]);
	assert_eq!(Token::Number(4.0), tokens[2]);
	assert_eq!(Token::Mul, tokens[3]);
	assert_eq!(Token::Number(6.0), tokens[4]);
    }

    #[test]
    fn scan_arith_div()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("2 / 6 / 2 / 8");

	assert_eq!(7, tokens.len());
	assert_eq!(Token::Number(2.0), tokens[0]);
	assert_eq!(Token::Div, tokens[1]);
	assert_eq!(Token::Number(6.0), tokens[2]);
	assert_eq!(Token::Div, tokens[3]);
	assert_eq!(Token::Number(2.0), tokens[4]);
	assert_eq!(Token::Div, tokens[5]);
	assert_eq!(Token::Number(8.0), tokens[6]);
    }
    
    #[test]
    fn scan_arith_mod()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("1 % 3 % 6");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(1.0), tokens[0]);
	assert_eq!(Token::Mod, tokens[1]);
	assert_eq!(Token::Number(3.0), tokens[2]);
	assert_eq!(Token::Mod, tokens[3]);
	assert_eq!(Token::Number(6.0), tokens[4]);
    }
    
    // scan cmp //
    #[test]
    fn scan_cmp_ltle()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("1 lt 3 le 6");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(1.0), tokens[0]);
	assert_eq!(Token::Lt, tokens[1]);
	assert_eq!(Token::Number(3.0), tokens[2]);
	assert_eq!(Token::Le, tokens[3]);
	assert_eq!(Token::Number(6.0), tokens[4]);
    }

    #[test]
    fn scan_cmp_ltle_par()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("(1 lt 3 le 6)");

	assert_eq!(7, tokens.len());
	assert_eq!(Token::OpenPar, tokens[0]);
	assert_eq!(Token::Number(1.0), tokens[1]);
	assert_eq!(Token::Lt, tokens[2]);
	assert_eq!(Token::Number(3.0), tokens[3]);
	assert_eq!(Token::Le, tokens[4]);
	assert_eq!(Token::Number(6.0), tokens[5]);
	assert_eq!(Token::ClosePar, tokens[6]);
    }

    #[test]
    fn scan_cmp_gtge()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("4 gt 2 ge 0");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(4.0), tokens[0]);
	assert_eq!(Token::Gt, tokens[1]);
	assert_eq!(Token::Number(2.0), tokens[2]);
	assert_eq!(Token::Ge, tokens[3]);
	assert_eq!(Token::Number(0.0), tokens[4]);
    }

    #[test]
    fn scan_cmp_eqne()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("7 eq 2 ne 8");

	assert_eq!(5, tokens.len());
	assert_eq!(Token::Number(7.0), tokens[0]);
	assert_eq!(Token::Eq, tokens[1]);
	assert_eq!(Token::Number(2.0), tokens[2]);
	assert_eq!(Token::Ne, tokens[3]);
	assert_eq!(Token::Number(8.0), tokens[4]);
    }

    #[test]
    fn scan_cmp_not()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("not 7");

	assert_eq!(2, tokens.len());
	assert_eq!(Token::Not, tokens[0]);
	assert_eq!(Token::Number(7.0), tokens[1]);
    }

    #[test]
    fn scan_cmp_true_simple()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("true");

	assert_eq!(1, tokens.len());
	assert_eq!(Token::True, tokens[0]);
    }

    #[test]
    fn scan_cmp_true_op()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("true eq true");

	assert_eq!(3, tokens.len());
	assert_eq!(Token::True, tokens[0]);
	assert_eq!(Token::Eq, tokens[1]);
	assert_eq!(Token::True, tokens[2]);
    }

    #[test]
    fn scan_cmp_false_simple()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("false");

	assert_eq!(1, tokens.len());
	assert_eq!(Token::False, tokens[0]);
    }

    #[test]
    fn scan_cmp_false_op()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("false eq false");

	assert_eq!(3, tokens.len());
	assert_eq!(Token::False, tokens[0]);
	assert_eq!(Token::Eq, tokens[1]);
	assert_eq!(Token::False, tokens[2]);
    }
    
    // scan group
    #[test]
    fn scan_group_pars()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("()");

	assert_eq!(2, tokens.len());
	assert_eq!(Token::OpenPar, tokens[0]);
	assert_eq!(Token::ClosePar, tokens[1]);
    }

    #[test]
    fn scan_string()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("kazog");

	assert!(match tokens[0].clone() {
	    Token::String(s) => {
		s == "kazog"
	    },

	    _ => { false }
	});
    }

        #[test]
    fn scan_string_multiple()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("kazog world how are you today");

	assert!(match tokens[0].clone() {
	    Token::String(s) => {
		s == "kazog world how are you today"
	    },

	    _ => { false }
	});
    }

    #[test]
    fn scan_ip()
    {
	let lexer = Lexer::new();
	let tokens : Vec<Token> = lexer.scan("192.168.0.1");

	assert!(match tokens[0].clone() {
	    Token::String(s) => {
		s == "192.168.0.1"
	    },

	    _ => { false }
	});
    }
    
    // error cases //
    #[test]
    #[should_panic]
    fn scan_err_var_no_name()
    {
	let lexer = Lexer::new();
	lexer.scan("$");
    }

    #[test]
    #[should_panic]
    fn scan_err_ip_double_period()
    {
	let lexer = Lexer::new();
	lexer.scan("192.168..0.1");
    }

    #[test]
    #[should_panic]
    fn scan_err_ip_not_enough_digit()
    {
	let lexer = Lexer::new();
	lexer.scan("192.168.0");
    }

    #[test]
    #[should_panic]
    fn scan_err_ip_too_many_digits()
    {
	let lexer = Lexer::new();
	lexer.scan("192.168.0.1.14");
    }

    #[test]
    #[should_panic]
    fn scan_err_ip_start_with_period()
    {
	let lexer = Lexer::new();
	lexer.scan(".192.168.0.1");
    }

    #[test]
    #[should_panic]
    fn scan_err_ip_end_with_period()
    {
	let lexer = Lexer::new();
	lexer.scan("192.168.0.1.");
    }
}
