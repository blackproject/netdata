pub mod symtable;
mod tokens;
mod lexer;
mod parser;
mod evaluator;
use tokens::Token;

#[derive(Debug)]
#[derive(Clone)]
#[derive(PartialEq)]
pub struct ParseTree
{
    token : Token,
    children : Vec<Box<ParseTree>>
}

pub fn evaluate(symtable : &symtable::SymTable, text : &str) -> String
{
    let lexer = lexer::Lexer::new();

    let tokens : Vec<Token> = lexer.scan(text);
    
    let mut parser = parser::Parser::new( tokens );

    let mut evaluator = evaluator::Evaluator::new( (*symtable).clone(), parser.parse() );
    
    return evaluator.evaluate();
}

pub fn interpolate(symtable : &symtable::SymTable, text : &str) -> String
{
    let mut new_text = String::from(text);
    
    for key in symtable.map.keys()
    {
	new_text = new_text.replace(format!("${}", key).as_str(), symtable.get(key).unwrap().as_str());
    }

    if new_text.contains('$')
    {
	panic!("Undefined variable.");
    }
    
    return new_text;
}

/********/
/* TEST */
/********/

#[cfg(test)]
mod tests
{
    /////////////////////////
    // Interpolation::Var //
    ////////////////////////
    
    // Nominals cases
    #[test]
    fn interpolate_var_simple_number()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "42");
	
	assert_eq!("42", super::interpolate(&symtable, "$x"));
    }

    #[test]
    fn interpolate_var_simple_string()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("name", "Peter Tosh");
	
	assert_eq!("Peter Tosh", super::interpolate(&symtable, "$name"));
    }

    #[test]
    fn interpolate_var_with_text_around()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("age", "24");
	
	assert_eq!("I am 24 years old", super::interpolate(&symtable, "I am $age years old"));
    }

    #[test]
    fn interpolate_var_no_var()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "42");
	
	assert_eq!("abc def", super::interpolate(&symtable, "abc def"));
    }

    // Error cases
    #[test]
    #[should_panic(expected = "Undefined variable.")]
    fn interpolate_var_undefined()
    {
	let symtable = super::symtable::SymTable::new();
	
	super::interpolate(&symtable, "hello $name");
    }
    
    /////////////////////////
    // Interpolation::Cmp //
    ////////////////////////
    
    // Nominals cases
    #[test]
    fn interpolate_cmp()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("var", "32");
	
	assert_eq!("32 lt 34", super::interpolate(&symtable, "$var lt 34"));
	assert_eq!("32 le 31", super::interpolate(&symtable, "$var le 31"));
	assert_eq!("32 gt 36", super::interpolate(&symtable, "$var gt 36"));
	assert_eq!("32 ge 29", super::interpolate(&symtable, "$var ge 29"));
	assert_eq!("32 eq 42", super::interpolate(&symtable, "$var eq 42"));
	assert_eq!("32 ne 76", super::interpolate(&symtable, "$var ne 76"));
	assert_eq!("not 32", super::interpolate(&symtable, "not $var"));
    }
    
    // Error cases

    /////////////////////
    // Evaluation::Add //
    /////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_add_no_space()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "32");
	symtable.set("y", "12");
	
	assert_eq!("44", super::evaluate(&symtable, "$x+$y"));
    }

    #[test]
    fn evaluation_add_with_spaces()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "16");
	symtable.set("y", "17");
	
	assert_eq!("33", super::evaluate(&symtable, "           $x  +              $y           "));
    }
    
    // Error cases
    #[test]
    #[should_panic]
    fn evaluation_add_missing_left_op()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("y", "12");
	
	super::evaluate(&symtable, "+$y");
    }

    #[test]
    #[should_panic]
    fn evaluation_add_missing_right_op()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "12");
	
	super::evaluate(&symtable, "$x+");
    }

    /////////////////////
    // Evaluation::Sub //
    /////////////////////
    
    // Nominals cases
    
    // Error cases

    /////////////////////
    // Evaluation::Mul //
    /////////////////////
    
    // Nominals cases
    
    // Error cases

    /////////////////////
    // Evaluation::Div //
    /////////////////////
    
    // Nominals cases
    
    // Error cases
    #[test]
    #[should_panic(expected = "Division by zero.")]
    fn evaluation_div_by_zero()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "16");
	symtable.set("y", "0");
	
	super::evaluate(&symtable, "$x/$y");
    }

    /////////////////////
    // Evaluation::Mod //
    /////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_mod_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "23");
	symtable.set("y", "17");
	
	assert_eq!("6", super::evaluate(&symtable, "$x % $y"));
    }
    
    // Error cases
    #[test]
    #[should_panic(expected = "Modulo zero undefined.")]
    fn evaluation_mod_zero_undefined()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "23");
	symtable.set("y", "0");
	
	super::evaluate(&symtable, "$x % $y");
    }

    #[test]
    #[should_panic(expected = "Modulo with non integer undefined.")]
    fn evaluation_mod_non_int()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "23");
	symtable.set("y", "3.1");
	
	super::evaluate(&symtable, "$x % $y");
    }

    //////////////////////
    // Evaluation::USub //
    //////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_usub_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "7");
	symtable.set("y", "6");
	
	assert_eq!("1", super::evaluate(&symtable, "$x + -$y"));
	assert_eq!("-1", super::evaluate(&symtable, "-$x + $y"));
	assert_eq!("-13", super::evaluate(&symtable, "-$x + -$y"));
    }
    
    // Error cases

    ////////////////////
    // Evaluation::Lt //
    ////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_lt_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "3");
	
	assert_eq!("false", super::evaluate(&symtable, "$x lt 2"));
	assert_eq!("false", super::evaluate(&symtable, "$x lt 3"));
	assert_eq!("true", super::evaluate(&symtable, "$x lt 4"));
    }
    
    // Error cases

    ////////////////////
    // Evaluation::Le //
    ////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_le_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "42");
	
	assert_eq!("false", super::evaluate(&symtable, "$x le 41"));
	assert_eq!("true", super::evaluate(&symtable, "$x le 42"));
	assert_eq!("true", super::evaluate(&symtable, "$x le 43"));
    }

    // Error cases

    ////////////////////
    // Evaluation::Gt //
    ////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_gt_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "17");
	
	assert_eq!("true", super::evaluate(&symtable, "$x gt 16"));
	assert_eq!("false", super::evaluate(&symtable, "$x gt 17"));
	assert_eq!("false", super::evaluate(&symtable, "$x gt 18"));
    }
    
    // Error cases
    
    ////////////////////
    // Evaluation::Ge //
    ////////////////////
    
    // Nominals cases
        #[test]
    fn evaluation_ge_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("x", "55");
	
	assert_eq!("true", super::evaluate(&symtable, "$x ge 54"));
	assert_eq!("true", super::evaluate(&symtable, "$x ge 55"));
	assert_eq!("false", super::evaluate(&symtable, "$x ge 56"));
    }

    // Error cases

    ////////////////////
    // Evaluation::Eq //
    ////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_eq_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("var", "29");
	
	assert_eq!("false", super::evaluate(&symtable, "$var eq 28"));
	assert_eq!("true", super::evaluate(&symtable, "$var eq 29"));
	assert_eq!("false", super::evaluate(&symtable, "$var eq 30"));
    }

    // Error cases

    ////////////////////
    // Evaluation::Ne //
    ////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_ne_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("var", "46");
	
	assert_eq!("true", super::evaluate(&symtable, "$var ne 45"));
	assert_eq!("false", super::evaluate(&symtable, "$var ne 46"));
	assert_eq!("true", super::evaluate(&symtable, "$var ne 47"));
    }
    
    // Error cases

    /////////////////////
    // Evaluation::Not //
    /////////////////////
    
    // Nominals cases
    #[test]
    fn evaluation_not_simple()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("var", "46");
	
	assert_eq!("false", super::evaluate(&symtable, "not ($var ne 45)"));
	assert_eq!("true", super::evaluate(&symtable, "not ($var ne 46)"));
	assert_eq!("false", super::evaluate(&symtable, "not ($var ne 47)"));
    }
    
    // Error cases

    //////////////////////
    // Evaluation::True //
    //////////////////////
    #[test]
    fn evaluation_true_false_eq()
    {
	let symtable = super::symtable::SymTable::new();
	
	assert_eq!("true", super::evaluate(&symtable, "true eq true"));
	assert_eq!("false", super::evaluate(&symtable, "true eq false"));
	assert_eq!("false", super::evaluate(&symtable, "false eq true"));
	assert_eq!("true", super::evaluate(&symtable, "false eq false"));
    }

    ////////////
    // String //
    ////////////
    #[test]
    fn evaluation_string_eq()
    {
	let symtable = super::symtable::SymTable::new();
	
	assert_eq!("true", super::evaluate(&symtable, "bonjour eq bonjour"));
	assert_eq!("false", super::evaluate(&symtable, "aurevoir eq adieu"));
    }

    #[test]
    fn evaluation_string_neq()
    {
	let symtable = super::symtable::SymTable::new();
	
	assert_eq!("false", super::evaluate(&symtable, "salut ne salut"));
	assert_eq!("true", super::evaluate(&symtable, "bye ne bi"));
    }

    ////////////////
    // Precedence //
    ////////////////
    
    // Nominals cases
    #[test]
    fn precedence_add_mul()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("hello", "12");
	
	assert_eq!("18", super::evaluate(&symtable, "$hello + 2 * 3"));
	assert_eq!("12.666666666666666", super::evaluate(&symtable, "$hello + 2 / 3"));
	assert_eq!("6", super::evaluate(&symtable, "$hello - 2 * 3"));
	assert_eq!("11.333333333333334", super::evaluate(&symtable, "$hello - 2 / 3"));
	assert_eq!("10", super::evaluate(&symtable, "$hello - 2 % 3"));
    }

    #[test]
    fn precedence_cmp()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("hello", "12");
	
	assert_eq!("true", super::evaluate(&symtable, "$hello gt 10 == 1 lt 2"));
    }

    #[test]
    fn precedence_cmp_op()
    {
	let mut symtable = super::symtable::SymTable::new();
	symtable.set("hello", "-5");
	
	assert_eq!("true", super::evaluate(&symtable, "$hello + 1 eq -4"));
	assert_eq!("true", super::evaluate(&symtable, "$hello * 2 eq -10"));
	assert_eq!("true", super::evaluate(&symtable, "$hello + 5 ge 0"));
	assert_eq!("false", super::evaluate(&symtable, "$hello + 5 ge 1"));
    }
    
    // Error cases
}
