extern crate treexml;
extern crate regex;

use std::io::prelude::*;

mod interpreter;

fn main()
{
    let args : Vec<String> = std::env::args().skip(1).collect();
    let mut input = String::new();
    
    if args.len() > 0
    {
	let mut file = std::fs::File::open(args[0]
					   .clone())
	    .expect(format!("Cannot open file \"{}\"",
			    args[0])
		    .as_str());
	
	let mut buf : [u8;1] = [0;1];

	while file.read(&mut buf[..]).unwrap() > 0
	{
	    input.push_str(std::str::from_utf8(&buf[..]).unwrap());
	}
    }
    
    let mut interpreter = interpreter::Interpreter::new(input);

    interpreter.interpret();
}
