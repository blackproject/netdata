use super::ParseTree;
use super::tokens::Token;
use super::symtable::SymTable;

pub struct Evaluator
{
    tree: ParseTree,
    symtable: SymTable
}

impl Evaluator
{
    pub fn new(symtable : SymTable, tree : ParseTree) -> Evaluator
    {
	return Evaluator {
	    tree,
	    symtable
	};
    }

    pub fn evaluate(&mut self) -> String
    {
	return self.eval(&self.tree);
    }

    fn eval(&self, subtree : &ParseTree) -> String
    {
	match &subtree.token
	{
	    Token::Add => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		return format!("{}", left.parse::<f64>().unwrap() + right.parse::<f64>().unwrap());
	    },
	    
	    Token::Mul => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		return format!("{}", left.parse::<f64>().unwrap() * right.parse::<f64>().unwrap());
	    },
	    
	    Token::Sub => {
		if subtree.children.len() == 1
		{
		    let left : String = self.eval(&subtree.children[0]);
		    return format!("{}", -left.parse::<f64>().unwrap());
		}
		else
		{
		    let left : String = self.eval(&subtree.children[0]);
		    let right : String = self.eval(&subtree.children[1]);
		    return format!("{}", left.parse::<f64>().unwrap() - right.parse::<f64>().unwrap());
		}
	    },
	    
	    Token::Div => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		let r : f64  = right.parse::<f64>().unwrap();
		
		if r == 0.0
		{
		    panic!("Division by zero.");
		}
		
		return format!("{}", left.parse::<f64>().unwrap() / r);
	    },
	    
	    Token::Mod => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		let rf = right.parse::<f64>().unwrap();

		if rf != rf.floor()
		{
		    panic!("Modulo with non integer undefined.");
		}


		if rf == 0.0
		{
		    panic!("Modulo zero undefined.");
		}
		
		return format!("{}", left.parse::<f64>().unwrap() % right.parse::<f64>().unwrap());
	    },
	    
	    Token::GroupPar => {
		return self.eval(&subtree.children[0]);
	    },
	    
	    Token::Lt => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		return format!("{}", left.parse::<f64>().unwrap() < right.parse::<f64>().unwrap());
	    },
	    
	    Token::Le => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		return format!("{}", left.parse::<f64>().unwrap() <= right.parse::<f64>().unwrap());
	    },
	    
	    Token::Gt => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		return format!("{}", left.parse::<f64>().unwrap() > right.parse::<f64>().unwrap());
	    },
	    
	    Token::Ge => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		return format!("{}", left.parse::<f64>().unwrap() >= right.parse::<f64>().unwrap());
	    },
	    
	    Token::Eq => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		let l : Result<f64, std::num::ParseFloatError> = left.parse::<f64>();
		let r : Result<f64, std::num::ParseFloatError> = right.parse::<f64>();
		
		if l.is_err() && r.is_err()
		{
		    return format!("{}", left == right);
		}
		
		return format!("{}", l.unwrap() == r.unwrap());
	    },
	    
	    Token::Ne => {
		let left : String = self.eval(&subtree.children[0]);
		let right : String = self.eval(&subtree.children[1]);
		let l : Result<f64, std::num::ParseFloatError> = left.parse::<f64>();
		let r : Result<f64, std::num::ParseFloatError> = right.parse::<f64>();
		
		if l.is_err() && r.is_err()
		{
		    return format!("{}", left != right);
		}
		
		return format!("{}", l.unwrap() != r.unwrap());
	    },
	    
	    Token::Not => {
		let child : String = self.eval(&subtree.children[0]);
		if child != "true" && child != "false"
		{
		    if child.is_empty() { return "true".to_string(); }
		    else { return "false".to_string(); }
		}
		return format!("{}", if child == "true"  { "false" } else { "true" } );
	    },

	    Token::True => {
		return format!("true");
	    },

	    Token::False => {
		return format!("false");
	    },
	    
	    Token::Var(s) => {
		return format!("{}", self.symtable.get(s).unwrap());
	    },

	    Token::String(s) => {
		return format!("{}", s);
	    },
	    
	    Token::Number(f) => {
		return format!("{}", f);
	    }
	    
	    _ => {
		panic!("Cannot evaluate {:?}", &subtree.token);
	    }
	}
	
	
    }

}
