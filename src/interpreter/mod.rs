mod txtinterpreter;
use crate::interpreter::txtinterpreter::symtable::SymTable;
use std::io::prelude::*;

pub struct Interpreter
{
    input : String,
    symtable : SymTable,
    stream : Option<std::net::TcpStream>,
    closed : bool
}

impl Interpreter
{
    pub fn new(input : String) -> Interpreter
    {	
	return Interpreter {
	    input,
	    symtable: SymTable::new(),
	    stream: None::<std::net::TcpStream>,
	    closed: false
	};	    
    }

    pub fn interpret(&mut self)
    {
	let document = treexml::Document::parse(self.input.as_bytes()).unwrap();
	
	self.inter(&mut document.root.unwrap());
    }

    fn inter(&mut self, node : &treexml::Element)
    {
	match node.name.as_str()
	{
	    "app" => {
		for child in node.children.clone()
		{
		    self.inter(&child);
		}
	    },
	    
	    "write" => {
		self.inter_write(node);
	    },

	    "read" => {
		self.inter_read(node);
	    },

	    "set" => {
		self.inter_set(node);
	    },

	    "if" => {
		self.inter_if(node);
	    },

	    "loop" => {
		self.inter_loop(node);
	    },

	    "socket" => {
		self.inter_socket(node);
	    },

	    "listen" => {
		self.inter_listen(node);
	    },

	    "connect" => {
		self.inter_connect(node);
	    },

	    "receive" => {
		self.inter_receive(node);
	    },
	    
	    "send" => {
		self.inter_send(node);
	    },

	    "nbreceive" => {
		self.inter_non_blocking_receive(node);
	    },

	    "sleep" => {
		self.inter_sleep(node);
	    },
	    
	    "close" => {
		self.closed = true;
	    }
	    
	    _ => {
		panic!("Unknown tag {}", node.name.as_str());
	    }
	}
    }

    fn inter_write(&self, node : &treexml::Element)
    {
	let txt : String = node.text.as_ref().unwrap().to_string();
	println!("{}", txtinterpreter::interpolate(&self.symtable, txt.as_str()));
    }

    fn inter_read(&mut self, node : &treexml::Element)
    {
	let var : &str = node.attributes.get("var").unwrap();
	let mut val = String::new();

	let mut br = std::io::BufReader::new(std::io::stdin());
	let mut line = String::new();
	br.read_line(&mut line).unwrap();
	val.push_str(line.trim());
	
	self.symtable.set(var, val.as_str());
    }
    
    fn inter_set(&mut self, node : &treexml::Element)
    {
	let var : &str = node.attributes.get("var").unwrap();
	let val : &str = node.text.as_ref().unwrap().as_str();

	self.symtable.set(var, &txtinterpreter::evaluate(&self.symtable, val));
    }

    fn inter_if(&mut self, node : &treexml::Element)
    {
	let cond : &str = node.attributes.get("cond").unwrap();

	let eval : String = txtinterpreter::evaluate(&self.symtable, cond);

	if eval != "false" && eval != ""
	{
	    for child in &node.children
	    {
		self.inter(&child);
	    }
	}
    }

    fn inter_loop(&mut self, node : &treexml::Element)
    {
	let cond : &str = node.attributes.get("cond").unwrap();

	let mut eval = String::from("true");

	while eval != "false" && eval != ""
	{
	    for child in &node.children
	    {
		self.inter(&child);		
	    }

	    eval = txtinterpreter::evaluate(&self.symtable, cond);
	}
    }

    fn inter_socket(&mut self, node : &treexml::Element)
    {
	let mut target : String = node.attributes.get("target").unwrap().to_string();
	target = txtinterpreter::interpolate(&self.symtable, &mut target);
	let t : &str = target.as_str();
	self.symtable.set("host", t.split(":").next().unwrap());
	self.symtable.set("port", t.split(":").skip(1).next().unwrap());
	
	for child in &node.children
	{
	    self.inter(&child);
	}
    }

    fn inter_listen(&mut self, node : &treexml::Element)
    {
	let l = std::net::TcpListener::bind(format!("{}:{}",
						    self.symtable.get("host").unwrap(),
						    self.symtable.get("port").unwrap()));
	if l.is_ok()
	{
	    let listener = l.unwrap();
	    for stream in listener.incoming()
	    {
		self.stream = Some(stream.unwrap());
		
		for child in &node.children
		{
		    if !self.closed
		    {
			self.inter(&child);	
		    }
		}

		if self.closed
		{
		    self.closed = false;
		    self.stream = None;
		    return;
		}
		else
		{
		    self.closed = false;
		    self.stream = None;
		}
		
	    }
	}
    }

    fn inter_connect(&mut self, node : &treexml::Element)
    {
	let s = std::net::TcpStream::connect(format!("{}:{}",
						     self.symtable.get("host").unwrap(),
						     self.symtable.get("port").unwrap()));

	if s.is_ok()
	{
	    let stream = s.unwrap();
	    self.stream = Some(stream);
	    
	    for child in &node.children
	    {
		if !self.closed
		{
		    self.inter(&child);
		}
	    }

	    self.closed = false;
	}
    }

    fn inter_receive(&mut self, node : &treexml::Element)
    {	
	if let Some(stream) = &mut self.stream
	{
	    let var : &str = node.attributes.get("var").unwrap();
	    let mut buf : [u8;1] = [0;1];
	    let mut line = String::new();

	    while std::str::from_utf8(&buf).unwrap() != "\n"
	    {
		stream.read(&mut buf[..]).unwrap();
		line.push_str(std::str::from_utf8(&buf).unwrap());
	    }
	    
	    line = line.trim().to_string();
	    self.symtable.set(var, &line);
	}
    }

    fn inter_send(&mut self, node : &treexml::Element)
    {
	if let Some(stream) = &mut self.stream
	{
	    let txt : String = node.text.as_ref().unwrap().to_string();
	    let to_send : String = txtinterpreter::interpolate(&self.symtable, txt.as_str());
	    stream.write(to_send.as_bytes()).unwrap();
	    stream.write(b"\n").unwrap();
	}
    }

    fn inter_non_blocking_receive(&mut self, node : &treexml::Element)
    {	
	if let Some(stream) = &mut self.stream
	{
	    let var : &str = node.attributes.get("var").unwrap();
	    let mut buf : [u8;1] = [0;1];
	    let mut line = String::new();

	    stream.set_nonblocking(true).unwrap();
	    while std::str::from_utf8(&buf).unwrap() != "\n"
	    {
		if let Ok(_) = stream.read(&mut buf[..])
		{
		    line.push_str(std::str::from_utf8(&buf).unwrap());
		}
		else
		{
		    break;
		}
	    }
	    stream.set_nonblocking(false).unwrap();
	    line = line.trim().to_string();
	    self.symtable.set(var, &line);
	}
    }

    fn inter_sleep(&mut self, node : &treexml::Element)
    {
	let val = node.attributes.get("val").unwrap();
	let val_inter = txtinterpreter::evaluate(&self.symtable, val);
	let millis : u64 = val_inter.parse::<u64>().expect("Cannot wait for that time.");
	
	std::thread::sleep(std::time::Duration::from_millis(millis));

	for child in &node.children
	{
	    self.inter(&child)
	}
    }
}
