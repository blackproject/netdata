use super::tokens::Token;
use super::ParseTree;

#[derive(Debug)]
#[derive(PartialEq)]
pub struct Parser
{
    tokens: Vec<Token>,
    cursor : usize
	
}

impl Parser
{
    pub fn new(tokens : Vec<Token>) -> Parser
    {
	return Parser {
	    cursor: 0,
	    tokens
	};
    }

    pub fn parse(&mut self) -> ParseTree
    {
	self.cursor = 0;
	return self.expr();
    }

    fn expr(&mut self) -> ParseTree
    {
	return self.eqne();
    }

    fn eqne(&mut self) -> ParseTree
    {
	let mut eqne : ParseTree = self.cmp();

	while self.cursor < self.tokens.len() &&
	    (self.tokens[self.cursor] == Token::Eq ||
	     self.tokens[self.cursor] == Token::Ne)
	{
	    self.cursor += 1;
	    
	    eqne = ParseTree {
		token: self.tokens[self.cursor - 1].clone(),
		children: vec![Box::new(eqne), Box::new(self.cmp())]
	    };
	}
	
	return eqne;
    }

    fn cmp(&mut self) -> ParseTree
    {
	let mut cmp : ParseTree = self.not();

	while self.cursor < self.tokens.len() &&
	    (self.tokens[self.cursor] == Token::Lt ||
	     self.tokens[self.cursor] == Token::Le ||
	     self.tokens[self.cursor] == Token::Gt ||
	     self.tokens[self.cursor] == Token::Ge)
	{
	    self.cursor += 1;
	    
	    cmp = ParseTree {
		token: self.tokens[self.cursor - 1].clone(),
		children: vec![Box::new(cmp), Box::new(self.not())]
	    };
	}
	
	return cmp;
    }

    fn not(&mut self) -> ParseTree
    {	
	if self.cursor < self.tokens.len() &&
	    self.tokens[self.cursor] == Token::Not
	{
	    self.cursor += 1;
	
	    return ParseTree {
		token: self.tokens[self.cursor - 1].clone(),
		children: vec![Box::new(self.addsub())]
	    };
	}

	return self.addsub();
    }

    fn addsub(&mut self) -> ParseTree
    {
	let mut addsub : ParseTree = self.muldivmod();

	while self.cursor < self.tokens.len() &&
	    (self.tokens[self.cursor] == Token::Add ||
	     self.tokens[self.cursor] == Token::Sub)
	{
	    self.cursor += 1;
	    
	    addsub = ParseTree {
		token: self.tokens[self.cursor - 1].clone(),
		children: vec![Box::new(addsub), Box::new(self.muldivmod())]
	    };
	}
	
	return addsub;
    }

    fn muldivmod(&mut self) -> ParseTree
    {
	let mut muldivmod : ParseTree = self.usub();

	while self.cursor < self.tokens.len() &&
	    (self.tokens[self.cursor] == Token::Mul ||
	     self.tokens[self.cursor] == Token::Div ||
	     self.tokens[self.cursor] == Token::Mod)
	{
	    self.cursor += 1;
	    
	    muldivmod = ParseTree {
		token: self.tokens[self.cursor - 1].clone(),
		children: vec![Box::new(muldivmod), Box::new(self.usub())]
	    };
	}
	
	return muldivmod;
    }

    fn usub(&mut self) -> ParseTree
    {
	if self.cursor < self.tokens.len() &&
	    (self.tokens[self.cursor] == Token::Sub)
	{
	    self.cursor += 1;
	    
	    return ParseTree {
		token: self.tokens[self.cursor - 1].clone(),
		children: vec![Box::new(self.literal())]
	    };
	}
	
	return self.literal();
    }

    fn literal(&mut self) -> ParseTree
    {
	if self.cursor < self.tokens.len()
	{
	    if let Token::True = &self.tokens[self.cursor]
	    {
		self.cursor += 1;
		
		return ParseTree {
		    token: Token::True,
		    children: vec![]
		};
	    }

	    if let Token::False = &self.tokens[self.cursor]
	    {
		self.cursor += 1;
		
		return ParseTree {
		    token: Token::False,
		    children: vec![]
		};
	    }
	    
	    if let Token::Number(f) = &self.tokens[self.cursor]
	    {
		self.cursor += 1;
		
		return ParseTree {
		    token: Token::Number(*f),
		    children: vec![]
		};
	    }

	    if let Token::Var(s) = &self.tokens[self.cursor]
	    {
		self.cursor += 1;
		
		return ParseTree {
		    token: Token::Var(s.to_string()),
		    children: vec![]
		};
	    }

	    if let Token::String(s) = &self.tokens[self.cursor]
	    {
		self.cursor += 1;
		
		return ParseTree {
		    token: Token::String(s.to_string()),
		    children: vec![]
		};
	    }
	}	

	if self.tokens[self.cursor] == Token::OpenPar
	{
	    self.cursor += 1;

	    if self.tokens[self.cursor] == Token::ClosePar
	    {
		return ParseTree {
		    token: Token::GroupPar,
		    children: vec![]
		};
	    }
	    
	    return ParseTree {
		token: Token::GroupPar,
		children: vec![Box::new(self.expr())]
	    };
	}

	if self.cursor < self.tokens.len() && self.tokens[self.cursor] == Token::ClosePar
	{
	    self.cursor += 1;
	}
	
	panic!("Unknown literal : {:?}.", self.tokens[self.cursor]);
    }
}
/* EXPR	   := EQNE
 * 		   
 * EQNE	   := CMP ((Eq | Ne) CMP)*
 * CMP	   := NOT ((Lt | Le | Gt | Ge) NOT)*
 * NOT	   := Not? ADDSUB
 * 		   
 * ADDSUB	   := MULDIVMOD ((Add | Sub) MULDIVMOD)*
 * MULDIVMOD   := USUB ((Mul | Div | Mod) USUB)*
 * USUB	   := Sub? LITERAL
 * 		   
 * LITERAL	   := Number | Var | OpenPar EXPR ClosePar
 */

#[cfg(test)]
mod tests
{
    use super::Token;
    use super::Parser;
    use super::ParseTree;
    
    // parse_literal //
    #[test]
    fn parse_literal_number()
    {
	let tokens : Vec<Token> = vec![Token::Number(100.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {token: Token::Number(100.0), children: vec![]}, parser.parse());
    }

    #[test]
    fn parse_literal_var()
    {
	let tokens : Vec<Token> = vec![Token::Var("hello".to_string())];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {token: Token::Var(String::from("hello")), children: vec![]}, parser.parse());
    }

    #[test]
    fn parse_literal_empty_group()
    {
	let tokens : Vec<Token> = vec![Token::OpenPar, Token::ClosePar];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {token: Token::GroupPar, children: vec![]}, parser.parse());
    }

        #[test]
    fn parse_literal_group()
    {
	let tokens : Vec<Token> = vec![Token::OpenPar,
				       Token::Number(2.21),
				       Token::Add,
				       Token::Number(6.37),
				       Token::ClosePar];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::GroupPar,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Add,
		    children: vec![
			Box::new(ParseTree {
			    token: Token::Number(2.21),
			    children: vec![]
			}),
			Box::new(ParseTree {
			    token: Token::Number(6.37),
			    children: vec![]
			})
		    ]
		})
	    ]
	}, parser.parse());
    }

    // parse_arith //
    #[test]
    fn parse_arith_add_numbers()
    {
	let tokens : Vec<Token> = vec![Token::Number(1.0), Token::Add, Token::Number(2.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Add,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(1.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(2.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());	
    }

    #[test]
    fn parse_arith_sub_numbers()
    {
	let tokens : Vec<Token> = vec![Token::Number(7.3), Token::Sub, Token::Number(4.6)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Sub,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(7.3),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(4.6),
		    children: vec![]
		}),
	    ]
	}, parser.parse());	
    }

    #[test]
    fn parse_arith_mul_numbers()
    {
	let tokens : Vec<Token> = vec![Token::Number(33.1), Token::Mul, Token::Number(19.2)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Mul,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(33.1),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(19.2),
		    children: vec![]
		}),
	    ]
	}, parser.parse());	
    }

    #[test]
    fn parse_arith_div_numbers()
    {
	let tokens : Vec<Token> = vec![Token::Number(27.0), Token::Div, Token::Number(24.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Div,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(27.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(24.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());	
    }

    #[test]
    fn parse_arith_usub_numbers()
    {
	let tokens : Vec<Token> = vec![Token::Sub, Token::Number(34.5)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Sub,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(34.5),
		    children: vec![]
		})
	    ]
	}, parser.parse());	
    }

        #[test]
    fn parse_arith_usub_numbers_add()
    {
	let tokens : Vec<Token> = vec![Token::Number(12.0), Token::Add, Token::Sub, Token::Number(7.4)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Add,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(12.0),
		    children: vec![]
		}),
		
		Box::new(ParseTree {
		    token: Token::Sub,
		    children: vec![
			Box::new(ParseTree {
			    token: Token::Number(7.4),
			    children: vec![]
			})
		    ]
		})
	    ]
	}, parser.parse());	
    }
    
    #[test]
    fn parse_arith_op_var_and_numbers()
    {
	let tokens : Vec<Token> = vec![Token::Var("world".to_string()),
				       Token::Mul,
				       Token::Number(24.0),
				       Token::Add,
				       Token::Number(4.2)];
	
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Add,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Mul,
		    children: vec![
			Box::new(ParseTree {
			    token: Token::Var("world".to_string()),
			    children: vec![]
			}),
			
			Box::new(ParseTree {
			    token: Token::Number(24.0),
			    children: vec![]
			})
		    ]
		}),
		
		Box::new(ParseTree {
		    token: Token::Number(4.2),
		    children: vec![]
		}),
	    ]
	}, parser.parse());	
    }
    
    // parse_cmp //
    #[test]
    fn parse_cmp_lt()
    {
	let tokens : Vec<Token> = vec![Token::Number(11.0), Token::Lt, Token::Number(2.02)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Lt,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(11.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(2.02),
		    children: vec![]
		}),
	    ]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_le()
    {
	let tokens : Vec<Token> = vec![Token::Number(3.0), Token::Le, Token::Number(67.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Le,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(3.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(67.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_gt()
    {
	let tokens : Vec<Token> = vec![Token::Number(3.0), Token::Gt, Token::Number(67.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Gt,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(3.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(67.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_ge()
    {
	let tokens : Vec<Token> = vec![Token::Number(3.0), Token::Ge, Token::Number(67.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Ge,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(3.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(67.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_eq()
    {
	let tokens : Vec<Token> = vec![Token::Var("bob".to_string()), Token::Eq, Token::Number(67.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Eq,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Var("bob".to_string()),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(67.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_ne()
    {
	let tokens : Vec<Token> = vec![Token::Number(3.0), Token::Ne, Token::Number(67.0)];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Ne,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::Number(3.0),
		    children: vec![]
		}),

		Box::new(ParseTree {
		    token: Token::Number(67.0),
		    children: vec![]
		}),
	    ]
	}, parser.parse());
    }
    
    #[test]
    fn parse_cmp_not()
    {
	let tokens : Vec<Token> = vec![Token::Not, Token::OpenPar,
				       Token::Number(34.7), Token::Ne,
				       Token::Number(28.2),
				       Token::ClosePar];
	let mut parser = Parser::new(tokens);

	assert_eq!(ParseTree {
	    token: Token::Not,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::GroupPar,
		    children: vec![
			Box::new(ParseTree {
			    token: Token::Ne,
			    children: vec![
				Box::new(ParseTree {
				    token: Token::Number(34.7),
				    children: vec![]
				}),

				Box::new(ParseTree {
				    token: Token::Number(28.2),
				    children: vec![]
				})
			    ]
			})
		    ]
		})
	    ]
	}, parser.parse());
    }

     #[test]
    fn parse_cmp_true_simple()
    {
	let tokens : Vec<Token> = vec![Token::True];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::True,
	    children: vec![]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_not_true()
    {
	let tokens : Vec<Token> = vec![Token::Not, Token::True];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Not,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::True,
		    children: vec![]
		})
	    ]
	}, parser.parse());
    }
    
    #[test]
    fn parse_cmp_false_simple()
    {
	let tokens : Vec<Token> = vec![Token::False];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::False,
	    children: vec![]
	}, parser.parse());
    }

    #[test]
    fn parse_cmp_not_false()
    {
	let tokens : Vec<Token> = vec![Token::Not, Token::False];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Not,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::False,
		    children: vec![]
		})
	    ]
	}, parser.parse());
    }

    #[test]
    fn parse_string()
    {
	let tokens : Vec<Token> = vec![Token::String("hello".to_string())];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::String("hello".to_string()),
	    children: vec![]
	}, parser.parse());
    }

    #[test]
    fn parse_string_eq()
    {
	let tokens : Vec<Token> = vec![Token::String("hello".to_string()), Token::Eq, Token::String("world".to_string())];
	let mut parser = Parser::new(tokens);
	
	assert_eq!(ParseTree {
	    token: Token::Eq,
	    children: vec![
		Box::new(ParseTree {
		    token: Token::String("hello".to_string()),
		    children: vec![]
		}),
		
		Box::new(ParseTree {
		    token: Token::String("world".to_string()),
		    children: vec![]
		})
	    ]
	}, parser.parse());
    }
}    

