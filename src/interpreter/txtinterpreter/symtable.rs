#[derive(Clone)]
pub struct SymTable
{
    pub map : std::collections::HashMap<String, String>
}

impl SymTable
{
    pub fn new() -> SymTable
    {
	let mut st = SymTable {
	    map : std::collections::HashMap::new()
	};
	
	st.map.insert(String::from("host"), String::from("127.0.0.1"));
	st.map.insert(String::from("port"), String::from("8080"));
	
	return st;
    }
    
    pub fn set(&mut self, var : &str, val : &str)
    {
	self.map.insert(String::from(var), String::from(val));	 
    }

    pub fn get(&self, key : &str) -> Option<String>
    {
	if let Some(x) = self.map.get(&String::from(key))
	{
	    return Some(x.to_string());
	}
	
	return None;
    }
}


#[test]
fn test_initial_values()
{
    let table = SymTable::new();
    assert_eq!("127.0.0.1", table.get("host").unwrap());
    assert_eq!("8080", table.get("port").unwrap());
}

#[test]
fn test_add_var()
{
    let mut table = SymTable::new();

    assert_eq!(None, table.get("hello"));
    
    table.set("hello", "world");
    
    assert_eq!("world", table.get("hello").unwrap());
}
